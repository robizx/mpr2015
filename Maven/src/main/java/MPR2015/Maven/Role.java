package MPR2015.Maven;

import java.util.List;

public class Role {
	
	private String _roleName;
	private List<Permission> _permisions;
	
	public String getRoleName() {
		return _roleName;
	}
	
	public void setRoleName(String _roleName) {
		this._roleName = _roleName;
	}
	
	public List<Permission> getPermisions() {
		return _permisions;
	}
	
	public void setPermisions(List<Permission> _permisions) {
		this._permisions = _permisions;
	}
}
