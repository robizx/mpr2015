package MPR2015.Maven;

public class Address {
	
	private String _street;
	private String _buildingNumber;
	private String _flatNumber;
	private String _postalCode;
	private String _city;
	
	public String getStreet() {
		return _street;
	}
	
	public void setStreet(String _street) {
		this._street = _street;
	}
	
	public String getBuildingNumber() {
		return _buildingNumber;
	}
	
	public void setBuildingNumber(String _buildingNumber) {
		this._buildingNumber = _buildingNumber;
	}
	
	public String getFlatNumber() {
		return _flatNumber;
	}
	
	public void setFlatNumber(String _flatNumber) {
		this._flatNumber = _flatNumber;
	}
	
	public String getPostalCode() {
		return _postalCode;
	}
	
	public void setPostalCode(String _postalCode) {
		this._postalCode = _postalCode;
	}
	
	public String getCity() {
		return _city;
	}
	
	public void setCity(String _city) {
		this._city = _city;
	}
}
