package MPR2015.Maven;

import java.util.List;

public class Person extends User {
	
	private String _name;
	private String _surName;
	private List<Address> _addresses;
	private List<String> _phoneNumbers;
	private List<Role> _roles;
	
	public String getName() {
		return _name;
	}
	
	public void setName(String _name) {
		this._name = _name;
	}
	
	public String getSurName() {
		return _surName;
	}
	
	public void setSurName(String _surName) {
		this._surName = _surName;
	}
	
	public List<Address> getAddresses() {
		return _addresses;
	}
	
	public void setAddresses(List<Address> _addresses) {
		this._addresses = _addresses;
	}
	
	public List<String> getPhoneNumbers() {
		return _phoneNumbers;
	}
	
	public void setPhoneNumbers(List<String> _phoneNumbers) {
		this._phoneNumbers = _phoneNumbers;
	}
	
	public List<Role> getRoles() {
		return _roles;
	}
	
	public void setRoles(List<Role> _roles) {
		this._roles = _roles;
	}	
}
