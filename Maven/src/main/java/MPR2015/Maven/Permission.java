package MPR2015.Maven;

import java.util.List;

public class Permission {
	
	private List<String> _permissionName;

	public List<String> getPermissionName() {
		return _permissionName;
	}

	public void setPermissionName(List<String> _permissionName) {
		this._permissionName = _permissionName;
	}
}
