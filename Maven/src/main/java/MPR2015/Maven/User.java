package MPR2015.Maven;

public class User {
	
	protected String _login;
	protected String _pass;

	public String getLogin() {
		return _login;
	}

	public void setLogin(String _login) {
		this._login = _login;
	}
	
	public String getPass() {
		return _pass;
	}
	
	public void setPass(String _pass) {
		this._pass = _pass;
	}
}